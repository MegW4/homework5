﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Diagnostics;

namespace HW4
{
	public partial class MainPage : ContentPage
	{
		public MainPage()
		{
			InitializeComponent();
            NavigationPage.SetHasNavigationBar(this, false);
		}


        async void NavigateToTextCellListView(object sender, System.EventArgs e)
        {
            await Navigation.PushAsync(new ImageCellListView());
        }
    }
}
