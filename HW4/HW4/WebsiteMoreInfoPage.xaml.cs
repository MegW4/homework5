﻿using HW4.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using HW4.ViewModel;

namespace HW4
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class WebsiteMoreInfoPage : ContentPage
	{
		public WebsiteMoreInfoPage ()
		{
			InitializeComponent ();
		}

        public WebsiteMoreInfoPage(ImageCellItem monster)
        {
            InitializeComponent();
            BindingContext = monster;
        }

        void Handle_ItemTapped(object sender, Xamarin.Forms.ItemTappedEventArgs e)
        {
            var button = (Button)sender;
            ImageCellItem monster = (ImageCellItem)button.CommandParameter;
            var uri = new Uri(monster.url);
            Device.OpenUri(uri);
        }
    }
}